import path from 'path';
import webpack from 'webpack';
import webpackBaseConfig, { DEBUG } from './webpack.base.config.babel';

const AUTOPREFIXER_BROWSERS = [
  'last 2 versions',
  'not ie < 11',
  'safari >= 4',
];

export default {
  ...webpackBaseConfig,

  entry: path.resolve(__dirname, '../src/webClient/index.js'),
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'app.js',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'isomorphic-style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: DEBUG,
              minimize: !DEBUG,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              parser: 'postcss-scss',
              plugins: () => ([
                require('autoprefixer')({
                  browsers: AUTOPREFIXER_BROWSERS,
                }),
              ]),
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: DEBUG,
            },
          },
        ],
      },
      {
        test: /\.txt$/,
        loader: 'raw-loader',
      },
      {
        test: /\.svg$/,
        loader: 'svg-loader',
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'url-loader',
        query: {
          name: DEBUG ? '[path][name].[ext]' : '[hash].[ext]',
          limit: 10000,
        },
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url-loader?name=fonts/[name].[ext]&limit=65000&mimetype=application/font-woff',
      },
      {
        test: /\.(otf|ttf)$/,
        loader: 'url-loader?name=fonts/[name].[ext]&limit=65000&mimetype=application/octet-stream',
      },
      {
        test: /\.eot$/,
        loader: 'url-loader?name=fonts/[name].[ext]&limit=65000&mimetype=application/vnd.ms-fontobject',
      },
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.BROWSER': true,
    }),
  ],
  target: 'web',
  devtool: '#inline-source-map',
  resolve: {
    modules: [
      path.resolve(__dirname, '../src/webClient'),
      'node_modules',
    ],
    extensions: ['.js', '.jsx', '.json', '.webpack.js', '.web.js'],
  },
}