
const NODE_ENV = /development|production/gi.test(process.env.NODE_ENV)
  ? process.env.NODE_ENV.toLowerCase()
  : 'development';
const DEBUG = NODE_ENV === 'development'; // !process.argv.includes( '--release' );
const VERBOSE = NODE_ENV === 'development'; // process.argv.includes( '--verbose' );

export default {
  bail: !DEBUG,

  watch: DEBUG,

  cache: DEBUG,

  stats: {
    colors: true,
    reasons: false,
    hash: VERBOSE,
    version: VERBOSE,
    timings: true,
    chunks: false,
    chunkModules: VERBOSE,
    cached: VERBOSE,
    cachedAssets: VERBOSE,
    performance: true,
  },
}

export {
  DEBUG,
  VERBOSE,
  NODE_ENV,
};