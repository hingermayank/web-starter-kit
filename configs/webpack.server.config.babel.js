import path from 'path';
import fs from 'fs';
import webpack from 'webpack';
import webpackBaseConfig from './webpack.base.config.babel';

const nodeModules = {};
fs.readdirSync(path.resolve(__dirname, '../node_modules'))
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach(mod => {
    nodeModules[mod] = `commonjs ${mod}`;
  });

export default {
  ...webpackBaseConfig,

  entry: path.resolve(__dirname, '../src/server/app.js'),
  output: {
    path: path.resolve(__dirname, '../build'),
    filename: 'server.js'
  },
  mode: 'development',
  externals: nodeModules,
  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
    }]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.BROWSER': false,
    }),
  ],
  target: 'node',
  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  },
  devtool: '#inline-source-map',
}