import Ajax from '../utils/ajax';
import Promise from 'bluebird';


export const login = (providerType, accessToken) => {
  if (!providerType || !accessToken) {
    return new Promise.reject();
  }
  return Ajax.get(`/login/${providerType.toLowerCase()}?access_token=${accessToken}`);
};