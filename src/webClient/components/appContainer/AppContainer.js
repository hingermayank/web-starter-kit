import React from 'react';
import { BrowserRouter, Route } from "react-router-dom";
import LoginPage from '../loginPage';
import HomePage from '../homePage';


class App extends React.PureComponent {

  render() {
    return (
      <BrowserRouter>
        <section>
          <Route path="/login" component={LoginPage} />
          <Route path="/home" component={HomePage} />
        </section>
      </BrowserRouter>
    );
  }
}

export default App;
