import React from 'react';
import SocialLogin from 'react-social-login';
import {
  FacebookLoginButton as FacebookLoginBtn,
  GoogleLoginButton as GoogleLoginBtn,
  LinkedInLoginButton as LinkedInLoginBtn
} from "react-social-login-buttons";


const commonBtnProps = {
  style: { width: '250px' },
};

export const FacebookLoginButton = SocialLogin(({ children, triggerLogin, ...props }) => {
  return (
    <FacebookLoginBtn
      {...commonBtnProps}
      {...props}
      onClick={triggerLogin}
    />
  );
});

export const GoogleLoginButton = SocialLogin(({ children, triggerLogin, ...props }) => {
  return (
    <GoogleLoginBtn
      {...commonBtnProps}
      {...props}
      onClick={triggerLogin}
    />
  );
});

export const LinkedInLoginButton = SocialLogin(({ children, triggerLogin, ...props }) => {
  return (
    <LinkedInLoginBtn
      {...commonBtnProps}
      {...props}
      onClick={triggerLogin}
    />
  );
});

export default {
  FacebookLoginButton,
  GoogleLoginButton,
  LinkedInLoginButton
}
