import React from 'react';
import SocialLogin from 'react-social-login';
import { FacebookLoginButton, GoogleLoginButton, LinkedInLoginButton } from '../socialLoginButtons';
import { login } from '../../services/authorizationService';

const FacebookLoginBtn = SocialLogin(FacebookLoginButton);

class LoginPage extends React.PureComponent {

  handleLoginSuccess = user => {
    login(user._provider, user._token.accessToken)
      .then(() => {
        console.log('Loggedin');
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleLoginFailure = (error) => {
    console.log(error);
  };

  render() {
    const commonProps = {
      onLoginSuccess: this.handleLoginSuccess,
      onLoginFailure: this.handleLoginFailure,
    };
    return (
      <section className="">
        <FacebookLoginButton
          provider='facebook'
          appId='2284476658489474'
          {...commonProps}
        />
        <GoogleLoginButton
          provider='google'
          appId='200010362890-qbjtnsu2ed3qi3bonavehonr846hg806.apps.googleusercontent.com'
          {...commonProps}
        />
      </section >
    );
  }
}

export default LoginPage;
