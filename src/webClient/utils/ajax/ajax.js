import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import _curry from 'lodash/curry';
import _toUpper from 'lodash/toUpper';
import _isString from 'lodash/isString';
import _mapKeys from 'lodash/mapKeys';
import Promise from 'bluebird';
import restClient from './restClient';


function throwUnauthorized(err) {
  if (err && err.status === 401) {
    console.log('Unauth');
  }
  return err;
}

/**
 * Extract message from the error response from server
 * @param  {[type]} err [description]
 */
function getErrorMessage(err) {
  const responseText = _get(err.res, 'body') || _get(err.res, 'text');
  return _isString(responseText) ? responseText : responseText.message;
}

function errorHandler(err) {
  throwUnauthorized(err);
  return Promise.reject(getErrorMessage(err));
}

function successHandler(res) {
  return Promise.resolve(res.body || res.text);
}

export function makeMethods(_restInstance) {
  return {
    get: (...args) => _restInstance.get(...args).then(successHandler).catch(errorHandler),
    post: (...args) => _restInstance.post(...args).then(successHandler).catch(errorHandler),
    put: (...args) => _restInstance.put(...args).then(successHandler).catch(errorHandler),
    remove: (...args) => _restInstance.remove(...args).then(successHandler).catch(errorHandler),
  };
}

export function makeRestInstance(baseUrl, setupParams) {
  return restClient.create({ baseUrl, ...setupParams });
}

const restInstance = makeRestInstance('http://localhost:3000');

const restMethods = makeMethods(restInstance);

export const get = restMethods.get;

export const post = restMethods.post;

export const put = restMethods.put;

export const remove = restMethods.remove;

export default {
  get,
  post,
  put,
  remove,
};
