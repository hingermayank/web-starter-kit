import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/appContainer';

ReactDOM.render(<App />, document.getElementById('appContainer'));