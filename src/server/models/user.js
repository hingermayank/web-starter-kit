import mongoose from 'mongoose';

// we create a user schema
const userSchema = new mongoose.Schema({
  snId: {
    type: String,
    required: false,
    unique: true,
  },
  displayName: {
    type: String,
    required: false,
    trim: true
  },
  firstName: {
    type: String,
    required: false,
    trim: true
  },
  lastName: {
    type: String,
    required: false,
    trim: true
  },
  email: {
    type: String,
    required: false,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: false,
  },
  gender: {
    type: String,
    required: false,
  },
  createdAt: {
    type: Date,
    required: false
  },
  updatedAt: {
    type: Number,
    required: false
  },
  provider: {
    type: String,
    required: false
  }
}, { runSettersOnQuery: true }); // 'runSettersOnQuery' is used to implement the specifications in our model schema such as the 'trim' option.

userSchema.pre('save', function (next) {
  this.email = this.email ? this.email.toLowerCase() : '';

  const currentDate = new Date().getTime();
  this.updatedAt = currentDate;
  if (!this.createdAt) {
    this.createdAt = currentDate;
  }
  next();
});


const user = mongoose.model('user', userSchema);

export default user;