import { boot } from './services/bootService';
import { SERVER_CONFIG } from './configs/environment';
import authenticationRoutes from './routes/authentication';
import { isUserAuthenticated } from './middlewares/authentication';

const app = boot(SERVER_CONFIG);

app.use('/login', authenticationRoutes);
app.use(isUserAuthenticated());
app.get('/', (req, res) => {
  res.status(200).redirect('/home');
});


app.get('/home', (req, res) => {
  res.status(200).render('index');
});
