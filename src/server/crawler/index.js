import _castArray from 'lodash/castArray';
import defaultOptions from './constants/defaultOptions';

class WebCrawler {

  constructor() {
  }

  initCrawler() {
    this.options = defaultOptions || {};
    this.sources = [];
  }

  getOptions() {
    return this.options;
  }

  setOptions(options) {
    this.options = Object.assign(this.options, options);
    return this;
  }

  setSources(sources) {
    const _sources = _castArray(sources);
    this.sources.push(..._sources);
  }

}