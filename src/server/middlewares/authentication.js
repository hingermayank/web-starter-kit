import _get from 'lodash/get';
import { verifyAndGetPayload } from '../utils/jwt';
import { JWT_SECRET_KEY } from '../configs/environment';


const defaultErrorHandler = (req, res, next) => {
  res.status(403).redirect('/login');
}

export const isUserAuthenticated = (errorHandler = defaultErrorHandler) => (req, res, next) => {
  const token = _get(req.cookies, 'access_token');

  if (token) {
    const { decoded, error } = verifyAndGetPayload(token, JWT_SECRET_KEY) || {};
    if (error) {
      errorHandler(req, res, next);
    } else {
      req.user = decoded;
      next();
    }
  } else {
    errorHandler(req, res, next);
  }
}