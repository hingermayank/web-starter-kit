import jwt from 'jsonwebtoken';

const generateToken = (payload, secret, { expiresIn = '30d' } = {}) => {
  return jwt.sign(payload, secret, { expiresIn });
}

const verifyToken = (token, secret) => {
  return !!jwt.verify(token, secret);
}

const verifyAndGetPayload = (token, secret) => {
  try {
    return { decoded: jwt.verify(token, secret) };
  } catch (error) {
    return { error };
  }
}

module.exports = {
  generateToken,
  verifyToken,
  verifyAndGetPayload,
}
