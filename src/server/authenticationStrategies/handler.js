import _isEmpty from 'lodash/isEmpty';
import User from '../models/user';
import profileReader from '../readers/profile';

export function loginSuccessHandler(accessToken, refreshToken, profile, done) {
  User.findOne({ snId: profile.id }, function (err, user) {
    if (err) {
      return done(err);
    }
    if (!_isEmpty(user)) {
      return done(null, user);
    }
    const newUser = new User({
      displayName: profileReader.displayName(profile),
      firstName: profileReader.firstName(profile),
      lastName: profileReader.lastName(profile),
      email: profileReader.email(profile),
      snId: profileReader.id(profile),
      provider: profileReader.provider(profile),
    });
    newUser.save((error) => {
      if (error) {
        done(error);
        return;
      }
      return done(null, newUser);
    });
  });
}