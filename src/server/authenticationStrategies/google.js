import passport from 'passport';
import { Strategy } from 'passport-google-token';
import { AUTH_CHANNEL_APP_INFO } from '../configs/environment';
import { loginSuccessHandler } from './handler';

export const setGoogleStrategy = () => {
  passport.use(new Strategy({
    clientID: AUTH_CHANNEL_APP_INFO.GOOGLE.APP_ID,
    clientSecret: AUTH_CHANNEL_APP_INFO.GOOGLE.APP_SECRET,
  }, loginSuccessHandler));
}