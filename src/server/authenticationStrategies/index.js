import { setFacebookStrategy } from './facebook';
// import { setLinkedinStrategy } from './linkedin';
import { setGoogleStrategy } from './google';

export const setAuthenticationStrategies = () => {
  setFacebookStrategy();
  // setLinkedinStrategy();
  setGoogleStrategy();
}