import passport from 'passport';
import { Strategy } from 'passport-linkedin-token-oauth2';
import { AUTH_CHANNEL_APP_INFO } from '../configs/environment';
import { loginSuccessHandler } from './handler';


export const setLinkedinStrategy = () => {
  passport.use(new Strategy({
    clientID: AUTH_CHANNEL_APP_INFO.LINKEDIN.APP_ID,
    clientSecret: AUTH_CHANNEL_APP_INFO.LINKEDIN.APP_SECRET,
  }, loginSuccessHandler));
}
