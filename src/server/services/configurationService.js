import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import flash from 'connect-flash';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import User from '../models/user';
import { setAuthenticationStrategies } from '../authenticationStrategies';


export function configureAuthenticationServices() {
  setAuthenticationStrategies();
}


export function configureApp(app) {
  app.use(express.static(path.join(__dirname, './')));
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(passport.initialize());
  app.use(flash());
  app.set('views', path.resolve(__dirname, './views'));
  app.set('view engine', 'pug');
}