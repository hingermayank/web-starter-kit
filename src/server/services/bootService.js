import express from 'express';
import bodyParser from 'body-parser';
import flash from 'connect-flash';
import passport from 'passport';
import { configureAuthenticationServices, configureApp } from './configurationService';
import { connectToStore } from '../store/mongo';


export function boot(serverConfig) {
  const app = express();

  configureApp(app);
  configureAuthenticationServices();

  connectToStore();

  app.listen(serverConfig.port, () => {
    console.log(`Server started at port ${serverConfig.port}`);
  });

  return app;
}