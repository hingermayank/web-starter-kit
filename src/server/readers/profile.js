import _get from 'lodash/get';
import _property from 'lodash/property';

export default {
  id: _property('id'),

  firstName(profile) {
    return _get(profile, 'name.givenName');
  },

  lastName(profile) {
    return _get(profile, 'name.familyName');
  },

  displayName: _property('displayName'),

  email(profile) {
    return _get(profile, 'emails.0.value');
  },

  provider: _property('provider'),
}