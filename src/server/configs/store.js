export const MONGO_OPTIONS = {
  server: {
    socketOptions: { keepAlive: 1 },
  },
  auto_reconnect: true,
}