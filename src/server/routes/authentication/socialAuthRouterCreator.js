import express from 'express';
import passport from 'passport';
import { Router } from 'express';
import _pick from 'lodash/pick';
import { generateToken } from '../../utils/jwt';
import { JWT_SECRET_KEY } from '../../configs/environment';


export default {
  create(strategyType) {
    const router = Router();

    router.get('/', passport.authenticate(strategyType, { session: false }), (req, res, next) => {
      if (!req.user) {
        res.status(401).send({
          message: 'Failed to login. Please try again later.',
        });
      }
      const userFieldsToPick = _pick(req.user, ['_id', 'snId', 'provider', 'email', 'displayName', 'profileImgUrl']);
      const jwt = generateToken(userFieldsToPick, JWT_SECRET_KEY);
      res.cookie('access_token', jwt, { maxAge: 90000000000, expires: new Date(new Date() + 90000000000), secure: false, httpOnly: true });
      res.redirect('/home');
    });

    return router;
  },
}