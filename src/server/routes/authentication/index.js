import express from 'express';
import passport from 'passport';
import { Router } from 'express';
import socialAuthRouterCreator from './socialAuthRouterCreator';
import { isUserAuthenticated } from '../../middlewares/authentication';

const authRouter = express.Router();

const errorHandler = (req, res) => {
  res.render('index');
}

authRouter.get('/', isUserAuthenticated(errorHandler), (req, res) => {
  res.status(302).redirect('/home');
});

authRouter.use('/facebook', socialAuthRouterCreator.create('facebook-token'));
authRouter.use('/google', socialAuthRouterCreator.create('google-token'));
authRouter.use('/linkedin', socialAuthRouterCreator.create('linkedin-token'));

export default authRouter;