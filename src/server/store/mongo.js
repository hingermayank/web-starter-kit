import mongoose from 'mongoose';
import { DB_CONFIG } from '../configs/environment';
import { MONGO_OPTIONS } from '../configs/store';

mongoose.Promise = require('bluebird');

export function connectToStore() {
  mongoose.connect(DB_CONFIG.uri, MONGO_OPTIONS, (err, db) => {
    if (err) {
      console.log("Couldn't connect to database");
    } else {
      console.log(`Connected To Database`);
    }
  });
}

export default {
  connectToStore,
}